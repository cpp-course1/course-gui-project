@ECHO OFF

IF NOT EXIST "lib" (
    mkdir lib
    ECHO lib folder not found, creating it
) 

IF NOT EXIST "lib/wxWidgets" (
    cd lib
    git clone https://github.com/wxWidgets/wxWidgets.git --recurse-submodules
    rem ADD SOMETHIGN HERE ABOUT HTE MKLINK STUFF
    cd wxWidgets
    cd include
    cd wx
    cd msw
    cp ./setup0.h ../setup.h
    cd ..
    cd ..
    cd ..
    cd ..
    cd ..

)

IF NOT EXIST "build" (
    mkdir build
    ECHO creating build directory
) ELSE (
    ECHO build directory already exists
)

cd build
cmake .. -G "MinGW Makefiles"  -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++  -D wxBUILD_SHARED=OFF  -DCMAKE_CXX_FLAGS="-Wl,--subsystem,windows -mwindows"
make